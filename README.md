Altair 680 Basic
================

This is MITS Altair 680 BASIC version 1.1 rev 3.2, from 1976, the 6800
version (BASIC-68) of the [8080 Basic][]  written for MITS by Paul
Allen, Bill Gates and Monte Davidoff. The pre-initialization code size
is 1B04 bytes hex, 6916 bytes decimal.

It requires the Altair 680B monitor ROM (written by Paul Allen and
Mark Chamberlin), also included here. This is designed to be able to
be patched for different serial port configurations.

All files come from Michael Holley's SWTPC collection Altair Basic page;
the [original page][swtpc-ab] page now seems dead but the [copy at
archive.org][swtpc-ab-ar] may still have everything. Files containing
binary data are in Motorola [SREC] format with some additional textual
information at the top which needs to be removed for the monitor to load
it. These probably should not have `.TXT` extensions; I suspect that was
done to get the web server to serve them as text files.

The manual here is split across four files. A [single file
version][manual-ac] is also available, but it's not as good a scan.

[8080 Basic]: https://en.wikipedia.org/wiki/Altair_BASIC
[SREC]: https://en.wikipedia.org/wiki/SREC_(file_format)
[manual-ac]: http://altairclone.com/downloads/manuals/BASIC%20Manual%2075.pdf
[swtpc-ab]: http://www.swtpc.com/mholley/Altair/Altair_Basic.htm
[swtpc-ab-ar]: https://web.archive.org/web/20190616094721/http://www.swtpc.com:80/mholley/Altair/Altair_Basic.htm


Contents
--------

    Micro.txt       Source for Uiterwyck's Microbasic
    MITS_RAW.TXT    (SREC) Altair BASIC. Starts at 0000, which is entrypoint.
    disasm/         Disassembler configuration/build for MITS_RAW.TXT.
      README.md         Documentation start point for the disassembly
      basic680.dis      Disassembler output (generated file)
    manual/:        A scan of the Altair BASIC manual.
        Introduction.pdf
        GettingStarted.pdf
        BasicLanguage.pdf
        Appendices.pdf
    monitor/:       Altair 680B monitor ROM.
        PATCH.TXT       Patch for MP-S serial port.
        PROM_LST.TXT
        PROM_MON.TXT
        PROM_S1.TXT
    M6502.MAC       BASIC M6502 8K VER 1.1 BY MICRO-SOFT original source
    basic-80-5.2/   MS Basic-80 5.2 source downloaded from link below


Resources
---------

#### 6800 CPU

- [6800 Instruction Set Reference][6800ref]: Registers, addressing
  modes, details of each instruction and an opcode table.
- `mc10dis.asm`: Disassembly of Radio Shack MC-10 Color Computer BASIC,
  from [hackaday].

[6800ref]: http://www.8bit-era.cz/6800.html
[hackaday]: https://cdn.hackaday.io/files/1837077859720288/MC10%20Disassembly.txt

#### Other 6800 BASICs

- [Robert Uiterwyck's Micro Basic][uiterwyck]. 4K version published,
  along with a text editor, in 1976-06 SWTPC newsletter. Used MIKBUG
  for I/O. Later grew into 8K and disk versions.

[uiterwyck]: http://www.swtpc.com/mholley/NewsLetter1/MicroBasic.htm

#### General MS-BASIC Information

- [BASIC Memory Arrangement][basic3.2memory] from the Altair 3.2
  disassembly site. This gives a brief overview of the general
  arrangement of memory and other information on how the interpreter
  works.
- The Retrocomputing StackExchange tags [microsoft-basic][rctag-mb]
  and perhaps [applesoft-basic][rctag-ab] (also a form of MS basic)
  have fragments of useful information.
- The [pagetable.com BASIC tag][pt-basic] contains a lot of
  information about early MS basic, including source and several
  different annotated disassemblies of MS (particularly Commodore)
  BASICs.

[basic3.2memory]: http://altairbasic.org/int_ex.htm
[pt-basic]: https://www.pagetable.com/?cat=27
[rctag-ab]: https://retrocomputing.stackexchange.com/questions/tagged/applesoft-basic
[rctag-mb]: https://retrocomputing.stackexchange.com/questions/tagged/microsoft-basic

#### 8080 3.x MS BASICs (late '70s era)

The original 8080 source code was owned by MITS, though MS received
royalties on it, and [seems to have been lost][lost].

- [Altair BASIC 3.2 (4K) - Annotated Disassembly][basic3.2] is a
  disassembly of the 8080 version. This 6800 version was derived from
  the 8080 version (though probably the 8K BASIC) so this may provide
  useful information to help with the 6800 disassembly.
- [Ian's trip to Harvard][ian]. Notes from Ian Griffiths' trip to the
  Pusey Library at Harvard to see a listing of the original Altair 4K
  source. Not much detail, except for the order and purpose of
  routines in `F3.mac`.

[lost]: https://retrocomputing.stackexchange.com/a/25643/7208
[basic3.2]: http://altairbasic.org/
[ian]: http://altairbasic.org/other%20versions/ian.htm

#### 6502 MS BASICs

Ric Weiland, Bill Gates and Monte Davidoff at Microsoft wrote MOS 6502
BASIC in the summer of 1976 by converting the Intel 8080
version.[1][pt-create] This seems to be moderately heavily reorganized
because it was going in ROM instead of RAM and to add new features
since it grew past 8K of ROM anyway.

- [Micro-Soft BASIC M6502 8K source code][ms6502] (v1.1, 1978-07-27).
  This is the oldest version of MS BASIC known to be available online.
  This has been reformated. `PAGE` and `SUBTTL` keywords document the
  program organization; there's also _much_ more documentation in
  comments than in the newer MS 8080 source below. See "Reading the
  6502 Source" in [`disasm/basic680.md`](disasm/basic680.md) for more
  details.
- A [pagetable.com blog post][pt-ms6502] has a lot of information
  about the assembler (PDP-10 MACRO-10), build configurations and
  targets, source code organization, comparison with other versions,
  and documentation of some tricks such as the `$EA60` placeholder in
  self-modified code.
- [c64disasm.git] contains several annotated disassemblies of the C64
  BASIC ROM and KERNAL derived from the PET BASIC above. There's also
  an [online HTML version][c64disasm] that lists all the disassemblies
  side-by-side.
- [Applesoft Internal Structure][ca-ais] (Call A.P.P.L.E. 1982-01) has
  some excellent details on the structure of the interpreter,
  particularly keywords and parsing.
- [S-C DocuMentor: Applesoft][scdoc] is an annotated disassembly of
  Applesoft BASIC. The organization seems rather different from other
  MS BASICs.
- [mist64/msbasic] is an integrated assembly tree from which you can
  build 6502 MS-BASIC for any of ten different targets. The [Create
  your own Version of Microsoft BASIC for 6502][pt-create] on
  pagetable.com describes the details and history of 6502 MS-BASIC.
- [ancientcomputing/msbasic] is a branch of the above to build a version
  (based on the OSI build) for an RC2014-bus 6502 CPU board. The details of
  the modifications are given in the Ancient Computing blog post [MS BASIC
  for the RC2014 6502 CPU Part 2][acblog].

[ancientcomputing/msbasic]: https://github.com/ancientcomputing/msbasic
[acblog]: https://ancientcomputing.blogspot.com/2017/07/ms-basic-for-rc2014-6502-cpu-part-2.html
[c64disasm.git]: https://github.com/mist64/c64disasm
[c64disasm]: https://www.pagetable.com/c64disasm/
[ca-ais]: https://archive.org/details/DTCA2DOC-045_applesoft_internal/page/n1
[mist64/msbasic]: https://github.com/mist64/msbasic
[ms6502]: https://www.pagetable.com/docs/M6502.MAC.txt
[pt-create]: https://www.pagetable.com/?p=46
[pt-ms6502]: https://www.pagetable.com/?p=774
[scdoc]: http://www.txbobsc.com/scsc/scdocumentor/

#### 6809 MS BASICs

- [Microsoft BASIC for the Dragon 64][dragon]. GitHub repo with the only
  known source code for the Dragon 64 versions of the Microsoft 16K BASIC
  Interpreter for the Motorola 6809 (aka BASIC-69 and Extended Color
  BASIC). Includes shared and unique source for both the 32K and 64K mode
  ROMs. Recovered from a paper listing of the Motorola assembler output
  that was produced in 1983 at Dragon Data's R&D facility near their Kenfig
  factory in Port Talbot, Wales.

[dragon]: https://github.com/davidlinsley/DragonBasic

#### Newer MS BASICs

- [Disk Extended Basic 4.1 disassembly][disk4.1]. A barely-started
  disassembly of a later version of Altair basic. This does have [an
  explanation of keyword storage][disk4.1key] in the new alphabetic
  dispatch table format (not used by 3.x).
- [Microsoft Basic-80 5.2 Source][basic-80 5.2]. Microsoft source
  code, titled "BASIC Mpu 8080/8085/Z80/8086 (5.11)   /Bill Gates/Paul
  Allen". 1982 copyright date (in `INIT.MAC`). Though considerably
  newer and enhanced (e.g., it supports more than two-char variable
  names), it appears to retain substantial code from the original
  1975 version.

[basic-80 5.2]: https://winworldpc.com/download/c2bfc380-c2b2-c2b4-7d00-11c3a7c29d25
[disk4.1]: http://altairbasic.org/other%20versions/
[disk4.1key]: http://altairbasic.org/other%20versions/keywords.htm
