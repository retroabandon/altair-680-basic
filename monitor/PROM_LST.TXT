
                        NAM    PROM MOMITOR
                **
                **ALTAIR 680B PROM MONITOR
                **ACIA VERSION 1.0
                **
 0100           MIVEC   EQU    $100
 0104           NMIVEC  EQU    $104
 F002           STRAPS  EQU    $F002
 0000           NOTERM  EQU    0
 F000           ACIACS  EQU    $F000
 F001           ACIADA  EQU    $F001
                **
                **MONITOR STACK AND FLAGS
                **
 00F1                   ORG    $F1
 00F1           STACK   RMB    1         BOTTOM OF MONITOR'S STACK
 00F2           BRKADR  RMB    1         BREAKPOINT ADDRESS FLAG
 00F3           ECHO    RMB    1         TTY ECHO FLAG
 00F4           EXTFLG  RMB    1         ENTENDED CHARACTER FLAG
 00F5           BUFULL  RMB    1         BUFFER FULL FLAG
 00F6           SAVSTK  RMB    2         TEMP FOR STACK POINTER
 00F8           TEMP    RMB    1         TEMPORARY STORAGE
 00F9           BYTECT  RMB    1         BYTE COUNT
 00FA           XHI     RMB    1         XREG HIGH
 00FB           XLOW    RMB    1         XREG LOW
 00FC           SHIFT   RMB    1         BAUDOT SHIFT FLAG
 00FD           SAVEX   RMB    2         TEMP FOR INDEX RG
 00FF           BUFFER  RMB    1         BAUDOT CHARACTER FLAG
                **
                * START OF PROM
                **
 FF00                   ORG    $FF00
                **
                * INPUT ONE CHAR INTO A-REGISTER
                * ECHO CHARACTER IF BIT 7 OF ECHO FLAG IS CLEAR
                **
 FF00 8D 22     INCH    BSR    POLCAT    ACIA STATUC TO A REG
 FF02 24 FC             BCC    INCH      RECEIVE NOT READY
 FF04 C6 7F             LDA B  #$7F      MASK FOR PARITY REMOVAL
 FF06 D1 F3             CMP B  ECHO      CHECK ECHO FLAG
 FF08 F4 F0 01          AND B  ACIADA    GET CARACTER
 FF0B 24 74             BCC    OUTCH     ECHO
 FF0D 39                RTS              NO ECHO
                **
                * THE FOLLOWING NOP LINES LINES UP THE ENTRY
                * POINTS TO POLCAT IN THE TWO VERSIONS
                * OF THE MONITOR
                **
 FF0E 01                NOP
                **
                * INPUT ONE HEX DIGIT INTO B REG
                * RETURN TO CALLING PROGRAM IF
                * CHARACTER RECEIVED IS A HEX
                * DIGIT. IF NOT HEX, GO TO CRLF
                **
 FF0F 8D EF     INHEX   BSR    INCH      GET A CHARACTER
 FF11 C0 30             SUB B  #'0
 FF13 2B 3C             BMI    C1        NOT HEX
 FF15 C1 09             CMP B  #$9
 FF17 2F 0A             BLE    IN1HG     NOT HEX
 FF19 C1 11             CMP B  #$11
 FF1B 2B 34             BMI    C1        NOT HEX 
 FF1D C1 16             CMP B  #$16
 FF1F 2E 30             BGT    C1        NOT HEX
 FF21 C0 07             SUB B  #7        IT'S A LETTER-GET BCD
 FF23 39        IN1HG   RTS              RETURN
                **
                * POLE FOR CHARACTER
                * SETS CARRY IF CHARACTER IS IN BUFFER
                * CLOBBERS 8 REG
                **
 FF24 F6 F0 00  POLCAT  LDA B  ACIACS    ACIA STATUS T0 B
 FF27 57                ASR B            ROTATE RDRF BIT INTO CARRY
 FF28 39                RTS              RETURN
                **
                * LOAD PAPER TAPE
                * LOAD ONLY S1 TYPE RECORDS 
                * TERMINATE ON S9 OR CHECKSUM ERROR
                **
 FF29 8D D5     LOAD    BSR    INCH      READ FRAME
 FF2B C0 53             SUB B  #'S
 FF2D 26 FA             BNE    LOAD      FIRST CHAR NOT (S)
 FF2F 8D CF             BSR    INCH      READ FRAME
 FF31 C1 39             CMP B  #'9
 FF33 27 1C             BEQ    C1        S9 END OF FILE
 FF35 C1 31             CMP B  #'1
 FF37 26 F0             BNE    LOAD      SECOND CHAR NOT (1)
 FF39 4F                CLR A            ZERO THE CHECKSUM
 FF3A 8D 17             BSR    BYTE      READ BYTE
 FF3C C0 02             SUB B  #2
 FF3E D7 F9             STA B  BYTECT    BYTE COUNT
 FF40 8D 20             BSR    BADDR     GET ADDRESS OF BLOCK
 FF42 8D 0F     LOAD11  BSR    BYTE      GET DATA BYTE
 FF44 7A 00 F9          DEC    BYTECT    DECREMENT BYTE COUNT
 FF47 27 05             BEQ    LOAD15    DONE WITH THIS BLOCK
 FF49 E7 00             STA B  0,X       STORE DATA
 FF4B 08                INX              BUMP POINTER
 FF4C 20 F4             BRA    LOAD11    GO BACK FOR MORE
 FF4E 4C        LOAD15  INC A            INCREMENT CHECKSUM
 FF4F 27 D8     LLOAD   BEQ    LOAD      ALL OK - IT'S ZERO
 FF51 20 58     C1      BRA    CRLF      CHECKSUM ERROR - QUIT
                **
                * READ BYTE (2 HEX DIGITS)
                * INTO B REG
                * A IS USED FOR PAPER TAPE CHECKSUM
                **
 FF53 8D BA     BYTE    BSR    INHEX     GET FIRST HEX DIG
 FF55 58                ASL B            GET SHIFT TO HIGH ORDER 4 BITS
 FF56 58                ASL B
 FF57 58                ASL B
 FF58 58                ASL B
 FF59 1B                ABA              ADD TO CHEKSUM
 FF5A D7 F8             STA B  TEMP      STORE DIGIT
 FF5C 8D B1             BSR    INHEX     GET 2ND HEX DIG
 FF5E 1B                ABA              ADD TO CHECKSUM
 FF5F DB F8             ADD B  TEMP      COMBINE DIGITS TO GET BYTE
 FF61 39                RTS              RETURN
                **
                * READ 16 BIT ADDRESS INTO X
                * STORE SAME ADDRESS IN XHI & XLO
                * CLOBBERS B REG
                **
 FF62 8D EF     BADDR   BSR    BYTE      GET HIGH ORDER ADDRESS
 FF64 D7 FA             STA B  XHI       STORE IT
 FF66 8D EB             BSR    BYTE      GET LOW ORDER ADDRESS
 FF68 D7 FB             STA B  XLOW      STORE IT
 FF6A DE FA             LDX    XHI       LOAD X WITH ADDRESS BUILT
 FF6C 39                RTS              RETURN
                **
                * PRINT BYTE IN A REG
                * CLOBBERS B REG
                **
 FF6D 16        OUT2H   TAB              COPY BYTE TO B
 FF6E 54                LSR B            SHIFT TO RIGHT
 FF6F 54                LSR B
 FF70 54                LSR B
 FF71 54                LSR B
 FF72 8D 01             BSR    OUTHR     OUTPUT FIRST DIGIT
 FF74 16                TAB              BYTE INTO B AGAIN
 FF75 C4 0F     OUTHR   AND B  #$F       GET RID OF LEFT DIG
 FF77 CB 30             ADD B  #$30      GET ASCII
 FF79 C1 39             CMP B  #$39
 FF7B 23 04             BLS    OUTCH
 FF7D CB 07             ADD B  #7        IF IT'S A LETTER ADD 7
 FF7F 01                NOP              LINE UP OUTCH ENTRY POINTS
 FF80 01                NOP
 FF81 8C        OUTCH   FCB    $8C       USE CPX SKIP TRICK
 FF82 C6 20     OUTS    LDA B  #$20      OUTS PRINTS A SPACE
                **
                * OUTCH OUTPUTS CHARACTER IN B
                **
 FF84 37                PSH B            SAVE CHAR
 FF85 8D 9D     OUTC1   BSR    POLCAT    ACIA STATUS TO B REG
 FF87 57                ASR B
 FF88 24 FB             BCC    OUTC1     XMIT NOT READY
 FF8A 33                PUL B            CHAR BACK TO B REG
 FF8B F7 F0 01          STA B  ACIADA    OUTPUT CHARACTER
 FF8E 39                RTS
                **
                * EXAMINE AND DEPOSIT NEXT
                * USES CONTENTS OF XHI & XLO AS POINTER
                **
 FF8F DE FA     NCHANG  LDX    XHI       INCREMENT POINTER
 FF91 08                INX
 FF92 DF FA             STX    XHI
 FF94 96 FA             LDA A  XHI
 FF96 8D D5             BSR    OUT2H     PRINT OUT ADDRESS
 FF98 96 FB             LDA A  XLOW
 FF9A 8D D1             BSR    OUT2H
 FF9C 8C                FCB    $8C       USE CPX SKIP TRICK
                **
                * EXAMINE & DEPOSIT
                **
 FF9D 8D C3     CHANGE  BSR    BADDR     BUILD ADDRESS
 FF9F 8D E1             BSR    OUTS      PRINT SPACE
 FFA1 A6 00             LDA A  X         BYTE INTO A
 FFA3 8D C8             BSR    OUT2H     PRINT BYTE
 FFA5 8D DB             BSR    OUTS      PRINT SPACE
 FFA7 8D AA             BSR    BYTE      GET NEW BYTE
 FFA9 E7 00             STA B  0,X       STORE NEW BYTE
                **
                * COMMAND DECODING SECTION
                **
 FFAB 9E F6     CRLF    LDS    SAVSTK
 FFAD C6 0D             LDA B  #$D       CARRIAGE RETURN
 FFAF 8D D0             BSR    OUTCH
 FFB1 C6 0A             LDA B  #$A       LINE FEED
 FFB3 8D CC             BSR    OUTCH
 FFB5 C6 2E             LDA B  #'.       PROMPT CHARACTER
 FFB7 8D C8             BSR    OUTCH
 FFB9 BD FF 00          JSR    INCH      READ CHARACTER
 FFBC 17                TBA              MAKE A COPY
 FFBD 8D C3             BSR    OUTS      PRINT SPACE
 FFBF 81 4C             CMP A  #'L
 FFC1 27 8C             BEQ    LLOAD     LOAD PAPER TAPE
 FFC3 81 4A             CMP A  #'J
 FFC5 26 04             BNE    NOTJ
 FFC7 8D 99             BSR    BADDR     GET ADDRESS TO JUMP TO
 FFC9 6E 00             JMP    0,X       JUMP TO IT
 FFCB 81 4D     NOTJ    CMP A  #'M
 FFCD 27 CE             BEQ    CHANGE    EXAMINE & DEPOSIT
 FFCF 81 4E             CMP A  #'N
 FFD1 27 BC             BEQ    NCHANG    E & D NEXT
 FFD3 81 50             CMP A  #'P
 FFD5 26 D4             BNE    CRLF
 FFD7 3B                RTI              PROCEDE FROM BREAKPOINT
                **
                * RESET ENTRY POINT
                **
 FFD8 8E 00 F3  RESET   LDS    #ECHO     INITIALIZE STACK POINTER
 FFDB C6 03             LDA B  #3        INIT ECHO AND BRKADR FLAGS
 FFDD 37                PSH B
 FFDE 37                PSH B
 FFDF F7 F0 00          STA B  ACIACS    MASTER RESET ACIA
 FFE2 F6 F0 02          LDA B  STRAPS    LOOK AT STRAPS
 FFE5 2B 19             BMI    NOTERM    NO TERM - JUMP TO 0
 FFE7 C4 04             AND B  #4        GET # OF STOP BITS
 FFE9 CA D1             ORA B  #$D1
 FFEB F7 F0 00          STA B  ACIACS    INIT ACIA PORT
                **
                * SOFTWARE INTERRUPT ENTRY POINT
                **
 FFEE 9F F6     INTRPT  STS    SAVSTK    SAVE STACK POINTER
 FFF0 9F FA             STS    XHI       SAVE SP FOR N COMMAND
 FFF2 D6 F2             LDA B  BRKADR    IF BIT 7 OF BRKADR IS SET
 FFF4 2B 0A             BMI    NOTERM    JUMP TO 0
 FFF6 20 B3             BRA    CRLF      GOTO COMMAND DECODER
                **
                * NOW COME THE INTERRUPT VECTORS
                **
 FFF8                   ORG    $FFF8
 FFF8 01 00             FDB    MIVEC     MI VECTOR
 FFFA FF EE             FDB    INTRPT    SWI VECTOR
 FFFC 01 04             FDB    NMIVEC    NMI VECTOR
 FFFE FF D8             FDB    RESET     RESET VECTOR
                        END    
       
NO ERROR(S) DETECTED

   SYMBOL TABLE:

ACIACS F000   ACIADA F001   BADDR  FF62   BRKADR 00F2   BUFFER 00FF   
BUFULL 00F5   BYTE   FF53   BYTECT 00F9   C1     FF51   CHANGE FF9D   
CRLF   FFAB   ECHO   00F3   EXTFLG 00F4   IN1HG  FF23   INCH   FF00   
INHEX  FF0F   INTRPT FFEE   LLOAD  FF4F   LOAD   FF29   LOAD11 FF42   
LOAD15 FF4E   MIVEC  0100   NCHANG FF8F   NMIVEC 0104   NOTERM 0000   
NOTJ   FFCB   OUT2H  FF6D   OUTC1  FF85   OUTCH  FF81   OUTHR  FF75   
OUTS   FF82   POLCAT FF24   RESET  FFD8   SAVEX  00FD   SAVSTK 00F6   
SHIFT  00FC   STACK  00F1   STRAPS F002   TEMP   00F8   XHI    00FA   
XLOW   00FB   
