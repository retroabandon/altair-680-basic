* f9dasm Disassembly configuration.
* Errors in this file will not be reported.
*
* See `basic680.md` for further information on the annotations
* added here.
*
* This modeline is convenient to run in the basic680.dis buffer.
*   vim: set autoread iskeyword-=-

********************************************************************
*   Label conventions:
*     .nnnn: used only locally between the nearest non-dot labels
*     _nnnn: local label interrupted by non-dot label
*     ?aaaa: uncertain name
*   Comments and labels in upper case are from other versions
*   of MS-BASIC; lower case are from the reverse engineer.

********************************************************************
*   Disassembly options

option 6800
option noconv
option cchar ;

*   These disable the address, hexdump and ASCII dump in comments to
*   the right of the disassembled code.
option noaddr
option nohex
option noasc

********************************************************************
*   Code annotations

*********************************
*   Monitor ROM

label       FF00        INCH
label       FF24        POLCAT
label       FF81        OUTCH
lcomment    0618        Poll input; carry set if char avail (in monitor)
lcomment    041F        Read char into A (in monitor)
lcomment    08AD        Output char in B (in monitor)

*********************************
*   START

label       0000        START
label       0004        RDYJSR
label       0005        RDYJSR+1
lcomment    0004        !!! INIT replaces with JSR READY (warm start)
data        000A-00BE

label       0078        stktmp
lcomment    0078        temp storage for S when using for copy

comment     00BF
*label      00BF        sub00BF         * Unknown subroutine

lcomment    00c7        This abs. address incremented above

label       00A3        copy_dsttop
label       00A4        copy_dsttop+1
lcomment    00A3        top of destination range for `copy`
label       00A5        copy_srctop
label       00A6        copy_srctop+1
lcomment    00A5        top of source range for `copy`
label       00A7        copy_dstbot
label       00A8        copy_dstbot+
lcomment    00A7        bottom of dest range for copy
lcomment    00A7       .  (filled in at end of `copy`)
label       00A9        copy_srcbot
label       00AA        copy_srcbot+1
lcomment    00A9        1 below bottom of dest range for `copy`

comment     00DD
comment     00DD        Another data section?
data        00DD-0111

comment     0112
lcomment    0112        !!! INIT replaces dest with STROUT
label       0112        x0112
label       0113        x0112+1

data        0115-0251

comment     0115
data        0115-023A                   * to 023B?
comment     0119

*   Keyword table. Note that this changed in v5.2; that uses an
*   alphabetic dispatch table, ALPTAB, with sub-tables for each letter
*   (starting at RESLST: ATAB, BTAB, etc.). This ties in somehow with
*   STMDSP which is the statement dispatch table.
comment     015C
comment     015C        Reserved word tokens.
comment     015C        The high bit is set on the last char of each token.
label       015C        RESLST
char        015C-0233
*   The lcomment annotations here put each token on its own line.
*   The comment text isn't strictly necessary, but makes the source easier to
*   read if you use a version of f9dasm without the char-high-bit-set patch.
lcomment    015C        END
lcomment    015F        FOR
lcomment    0162        NEXT
lcomment    0166        DATA
lcomment    016A        INPUT
lcomment    016f        DIM
lcomment    0172        READ
lcomment    0176        LET
lcomment    0179        GOTO
lcomment    017D        RUN
lcomment    0180        IF
lcomment    0182        RESTORE
lcomment    0189        GOSUB
lcomment    018E        RETURN
lcomment    0194        REM
lcomment    0197        STOP
lcomment    019B        ON
lcomment    019D        NULL
lcomment    01A1        WAIT
lcomment    01A5        DEF
lcomment    01A8        POKE
lcomment    01AC        PRINT
lcomment    01B1        CONT
lcomment    01B5        LIST
lcomment    01B9        CLEAR
lcomment    01BE        NEW
lcomment    01C1        TAB(
lcomment    01C5        TO
lcomment    01C7        FN
lcomment    01C9        SPC(
lcomment    01CD        THEN
lcomment    01D1        NOT
lcomment    01D4        STEP
lcomment    01D8        +
lcomment    01D9        -
lcomment    01DA        \*
lcomment    01DB        /
lcomment    01DC        ^
lcomment    01DD        AND
lcomment    01E0        OR
lcomment    01E2        >
lcomment    01E3        =
lcomment    01E4        <
lcomment    01E5        SGN
lcomment    01E8        INT
lcomment    01EB        ABS
lcomment    01EE        USR
lcomment    01F1        FRE
lcomment    01F4        POS
lcomment    01F7        SQR
lcomment    01FA        RND
lcomment    01FD        LOG
lcomment    0200        EXP
lcomment    0203        COS
lcomment    0206        SIN
lcomment    0209        TAN
lcomment    020C        ATN
lcomment    020F        PEEK
lcomment    0213        LEN
lcomment    0216        STR$
lcomment    021A        VAL
lcomment    021D        ASK
lcomment    0220        CHR$
lcomment    0224        LEFT$
lcomment    0229        RIGHT$
lcomment    022F        MID$
lcomment    0233        Terminate statement table.

comment     0234

comment     023E

comment     02CE
comment     02CE       .  Fast data copy. This works downward through the range from copy_srctop
comment     02CE       .  through copy_srcbot + 1, copying the data to copy_dsttop downward.
comment     02CE       .  When the copy is complete, copy_dstbot will be filled in with the
comment     02CE       .  address of the last destination byte copied.
comment     02CE       . 
comment     02CE       .  This uses SP during the copy, so, though it disables interrupts, an NMI
comment     02CE       .  during the copy may still cause problems if it happens too close to the
comment     02CE       .  end of the copy (thus corrupting data below the bottom of the destination
comment     02CE       .  range) or if the NMI does not RTI but instead continues on with other code
comment     02CE       .  without resetting SP to a known good area for the stack.
comment     02CE       . 
label       02CE        copy
lcomment    02D0        save flags
label       02D0        copy1
lcomment    02D4        prevent INT (not NMI!) from using SP
label       02DA        _cpnxt
lcomment    02E5        restore stack
lcomment    02E7        restore flags, which also
lcomment    02E8       .  re-enables interrupts

comment     02EA
comment     02F0       .      ; fallthrough
comment     02F0

label       0302        \.rts302

comment     0303

comment     0311

comment     0500
label       0500        READY

*********************************
*   PRINT CODE

*           0834  related to output?

label       083E        CRFIN
label       084C        PRTRTS

*   STROUT
comment     0879        \ 
comment     0879        XXX This is a little strange in that IX appears to be
comment     0879        loaded with the location one BEFORE the start of the
comment     0879        string....
comment     0879        \ 
comment     0879        PRINT THE STRING POINTED TO BY IX-1 WHICH ENDS WITH A ZERO.
comment     0879        XXX ??? IF THE STRING IS BELOW DSCTMP IT WILL BE COPIED INTO STRING SPACE.
comment     0879        \ 
label       0879        STROUT
lcomment    0879        GET A STRING LITERAL
comment     087C        \ 
comment     087C        PRINT THE STRING WHOSE DESCRIPTOR IS POINTED TO BY FACMO.
comment     087C        \ 
label       087C        STRPRT
lcomment    087C        RETURN TEMP POINTER.
label       0880        STRPR2
lcomment    088C        TYPE REST OF CARRIAGE RETURN.
lcomment    088E        AND ON AND ON.

comment     0890        \ 
comment     0890        OUTDO OUTPUTS THE CHARACTER IN ACCA, USING CNTWFL
comment     0890        (SUPPRESS OR NOT), TRMPOS (PRINT HEAD POSITION),
comment     0890        TIMING, ETCQ. NO REGISTERS ARE CHANGED.
comment     0890        \ 
label       0890        OUTSPC
label       0895        OUTDO

*********************************
*   STRING FUNCTIONS

* XXX zero-page storage locations for string stuff below
* extra byte between DSCTMP len and addr, looks like?
label       0061        TEMPPT
*label      00AD        used by strini
label       00AF        tmpstrlen       * XXX maybe really start of DSCTMP?
label       00B0        DSCTMP
label       00B1        DSCTMP+1
label       00BB        STRNG1
label       00BD        STRNG2
label       0058        CHARAC
label       0059        ENDCHR


comment     0f01        \ 
comment     0f01        "STRINI" GET STRING SPACE FOR THE CREATION OF A STRING AND
comment     0f01        CREATES A DESCRIPTOR FOR IT IN "DSCTMP".
comment     0f01        \ 
label       0F01        STRINI

comment     0F0A
comment     0F0A        This entry point takes IX pointing to string value, instead of IX-1
label       0F0A        dstrlit

comment     0f0b        \ 
comment     0f0b        "STRLT2" TAKES THE STRING LITERAL WHOSE FIRST CHARACTER
comment     0f0b        IS POINTED TO BY IX-1 AND BUILDS A DESCRIPTOR FOR IT.
comment     0f0b        THE DESCRIPTOR IS INITIALLY BUILT IN "DSCTMP", BUT "PUTNEW"
comment     0f0b        TRANSFERS IT INTO A TEMPORARY AND LEAVES A POINTER
comment     0f0b        AT THE TEMPORARY IN FACMO&LO. THE CHARACTERS OTHER THAN
comment     0f0b        ZERO THAT TERMINATE THE STRING SHOULD BE SET UP IN "CHARAC"
comment     0f0b        AND "ENDCHR". IF THE TERMINATOR IS A QUOTE, THE QUOTE IS SKIPPED
comment     0f0b        OVER. LEADING QUOTES SHOULD BE SKIPPED BEFORE JSR. ON RETURN
comment     0f0b        THE CHARACTER AFTER THE STRING LITERAL IS POINTED TO
comment     0f0b        BY [STRNG2].
comment     0f0b        \ 
comment     0f0b        DSCTMP is temporary storage for a string descriptor,
comment     0f0b        one byte of length and two bytes of address.
comment     0f0b        \ 
label       0F0B        STRLIT
lcomment    0f0b        ASSUME STRING ENDS ON QUOTE.
char        0F0C
label       0F11        STRLIT2


lcomment    0F12        SAVE POINTER TO STRING.
lcomment    0F14        IN CASE NO STRCPY.
lcomment    0F16        INITIALIZE CHARACTER COUNT.
label       0F18        STRGET
lcomment    0F18        find 00 terminator or char after CHARAC or ENDCHR
lcomment    0F19        GET CHARACTER.
lcomment    0F1B        IF ZERO.
lcomment    0F1E        THIS TERMINATOR?
lcomment    0F20        YES
lcomment    0F24        LOOK FURTHER.
label       0F26        STRFIN
char        0F27
lcomment    0F2A        terminator not quote; back up to remove it
label       0F2B        STRFI2
lcomment    0F2D        ??? RETAIN COUNT

label       0F3A        PUTNEW
label       0F46        PUTNW1
label       0F43        ERRGO2

*                       XXX this comment needs 6809 update
comment     0F58
comment     0F58        \ 
comment     0F58        GETSPA - GET SPACE FOR CHARACTER STRING.
comment     0F58        MAY FORCE GARBAGE COLLECTION.
comment     0F58        \ 
comment     0F58        # OF CHARACTERS (BYTES) IN ACCA.
comment     0F58        RETURNS WITH POINTER IN [Y,X]. OTHERWISE (IF CAN'T GET
comment     0F58        SPACE) BLOWS OFF TO "OUT OF STRING SPACE" TYPE ERROR.
comment     0F58        ALSO PRESERVES [ACCA] AND SETS [FRESPC]=[Y,X]=PNTR AT SPACE.
comment     0F58        \ 
label       0F58        GETSPA
lcomment    0F58        SIGNAL NO GARBAGE COLLECTION YET.
label       0F5B        TRYAG2
lcomment    0F5B        SAVE FOR LATER.

comment     1025
label       1046       \.1046

comment     105F
label       1061        ?MOVSTR
lcomment    1061        save flags

label       107D        FREFAC

*********************************
*   Initialization

comment     18F9
label       18F9        INIT

lcomment    19FF        JSR instruction

*   Strings used at startup
comment     1A0B
comment     1A0B        Strings used by INIT routine
comment     1A0B        XXX Both high-byte and \\000 terminated?
data        1A0B-1B00

label       1A0A        FNS-1
label       1A0B        FNS             * WANT SIN-COS-TAN-ATN (trig functions)
char        1A1E
lcomment    1A1E        'N'|$80, \\000  * last char of string has high bit set

label       1A1F        AUTTXT-1        * Author string
label       1A20        AUTTXT
char        1A40-1A43
lcomment    1A40        '.'|$80, \\000

label       1A43        TTYWID-1
label       1A44        TTYWID          * Terminal width
char        1A51
lcomment    1A51        'H'|$80, \\000

label       1A52        WORDS-1
label       1A53        WORDS           * bytes free, version id, copyright 1976
char        1A5D
char        1A8A
char        1AA7

label       1AAA        MEMORY-1
label       1AAB        MEMORY          * Memory size
char        1AB5

label       1AB7        LASTWR

*   These locations appear to be garbage strings, perhaps left behind in
*   memory during editing. The region's contents are:
*       " IS IT RETURN",$0D
*       " BEQ OUT",$0D
*       " BRA INTAPE GET NEXT CHARACTER",$0D
*       " END"
*unused      1AB7-1AF4
*   13 (decimal) bytes of 00 fill out the page.
unused      1AF4-1B00

********************************************************************
*   Misc notes, work in progress, temp area

end

*   This does not remove the definition: MFFFF   EQU     $FFFF
*unlabel FFFF
