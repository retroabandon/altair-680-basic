Altair 680 BASIC Disassembly
============================

This uses [f9dasm] to generate a disassembly of the Altair 680 BASIC
binary code in `../MITS_RAW.TXT`.

Run `make` to generate `basic680.dis`. Though it's a generated file, it's
committed so that it can be viewed on the web; ensure that before commiting
changes to `basic680.info` you have not commented out the `noaddr`, `nohex`
and `noasc` options and run `make`. (Leaving any of those options commented
out will change almost every line in the `.dis` file, making for an
enormous diff.)

### Files

    Makefile        Code to generate the disassembly using f9dasm
    f9dasm/         Git submodule checkout of f9dasm
    basic680.md     Details and explanations of the disassembly
    basic680.mot    (generated) The binary in Motorola S-record format
    basic680.info   Configuration for the disassembly
    basic680.dis    (generated) The disassembly output of f9dasm



<!-------------------------------------------------------------------->
[f9dasm]: https://github.com/Arakula/f9dasm

